//
//  LocalizationSuportFIle.swift
//  LoclizationDemo
//
//  Created by Govind Kumar on 23/04/23.
//

import Foundation
import UIKit
var lanType: languagess = .english

enum languagess {
    case english
    case Hindi
    case Marathi
    case Gujrati
    
}

 
extension  UILabel{
    func localizationMange(keyname:String) -> String{
      
        switch lanType {
        case .english:
            self.text = keyname.localizableStirng(loc: "en")
        case .Hindi:
            self.text =  keyname.localizableStirng(loc: "hi")
        case .Marathi:
            self.text =  keyname.localizableStirng(loc: "mr")
        case .Gujrati:
            self.text =  keyname.localizableStirng(loc: "gu")
          
        }
        return ""
    }
}
extension  UIButton{
    func localizationMange(keyname:String) -> String{
         
        switch lanType {
        case .english:
            self.setTitle(keyname.localizableStirng(loc: "en"), for: .normal)
        case .Hindi:
            self.setTitle(keyname.localizableStirng(loc: "hi"), for: .normal)
        case .Marathi:
            self.setTitle(keyname.localizableStirng(loc: "mr"), for: .normal)
        case .Gujrati:
            self.setTitle(keyname.localizableStirng(loc: "gu"), for: .normal)
     
        }
        
        return ""
    }
}
extension  UITextField{
    func localizationMange(keyname:String) -> String{
         
        switch lanType {
        case .english:
            self.placeholder = keyname.localizableStirng(loc: "en")
        case .Hindi:
            self.placeholder = keyname.localizableStirng(loc: "hi")
        case .Marathi:
            self.placeholder = keyname.localizableStirng(loc: "mr")
            
        case .Gujrati:
            self.placeholder = keyname.localizableStirng(loc: "gu")
      
        }
        
        return ""
    }
}
extension  UITextView{
    func localizationMange(keyname:String) -> String{
         
        switch lanType {
        case .english:
            self.text = keyname.localizableStirng(loc: "en")
        case .Hindi:
            self.text =  keyname.localizableStirng(loc: "hi")
        case .Marathi:
            self.text =  keyname.localizableStirng(loc: "mr")
        case .Gujrati:
            self.text =  keyname.localizableStirng(loc: "gu")
          
        }
        return ""
    }
}


extension  String {
    func localizationMange() -> String{
         
        switch lanType {
        case .english:
            return self.localizableStirng(loc: "en")
        case .Hindi:
            return self.localizableStirng(loc: "hi")
        case .Marathi:
            return  self.localizableStirng(loc: "mr")
        case .Gujrati:
            return  self.localizableStirng(loc: "gu")
        
        }
        return ""
    }
}

extension String {
    func localizableStirng(loc:String) -> String{
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
         let bundle = Bundle(path: path!)
        print("path123", path)
        return  NSLocalizedString(self,bundle: bundle!, comment:"")
    }
}
