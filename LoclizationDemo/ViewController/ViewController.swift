//
//  ViewController.swift
//  LoclizationDemo
//
//  Created by Govind Kumar on 23/04/23.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblEmail:UILabel!
    @IBOutlet weak var lblPassword:UILabel!
    @IBOutlet weak var lblConfirmPwd:UILabel!
    @IBOutlet weak var tfName:UITextField!
    @IBOutlet weak var tfEmail:UITextField!
    @IBOutlet weak var tfPwd:UITextField!
    @IBOutlet weak var tfCOnfirmPwd:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func manageLocalization(){
         lblName.localizationMange(keyname: "PiP-6h-WtW.text")
         lblEmail.localizationMange(keyname: "ji6-vL-Clf.text")
         lblConfirmPwd.localizationMange(keyname: "sKt-nP-toD.text")
         lblPassword.localizationMange(keyname: "fHr-Cf-PTw.text")
        
        tfName.localizationMange(keyname: "Byb-Qc-q57.placeholder")
        tfEmail.localizationMange(keyname: "vaf-L1-UNA.placeholder")
        tfPwd.localizationMange(keyname: "2oS-mm-kb7.placeholder")
        tfCOnfirmPwd.localizationMange(keyname: "zfq-9v-QKI.placeholder")
        
        
        
    }
    
    @IBAction func sengmentAction(_ sender: UISegmentedControl){
        
        if sender.selectedSegmentIndex == 0 {
            lanType = .english
        }else if sender.selectedSegmentIndex == 1 {
            lanType = .Hindi
        }else if sender.selectedSegmentIndex == 2 {
            lanType = .Gujrati
        }else if sender.selectedSegmentIndex == 3 {
            lanType = .Marathi
        }
        
        manageLocalization()
    }
    
    
    
}

